const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "tofiga01",
    database: "rssparse", //uncomment on creating table
});

con.connect(function (err) {
    if(err) throw err;
    console.log("Connected!");
    /*con.query("CREATE DATABASE rssparse DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci", function (err,result) {
        if (err) throw err;
        console.log("Db created");
    });*/                         //comment on creating table
    let sql = "CREATE TABLE rss (id INT AUTO_INCREMENT PRIMARY KEY, feedTitle VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemTitle VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemLink VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemPubDate VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemCreator VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemContentSnippet TEXT(65535) CHARACTER SET utf8 COLLATE utf8_general_ci, itemGuid VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci, itemCategories TEXT(65535) CHARACTER SET utf8 COLLATE utf8_general_ci)";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Database created!");
    });
});
