const home = require('./routes/home');
const parsedRSS = require('./routes/parsedRSS');
const debug = require('debug')('app:startup');
const express = require('express');
const app = express();

app.set('view engine', 'pug');
app.set('views', './views');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use('/', home);
app.use('/parsedRSS/', parsedRSS);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port: ${port}`));