const express = require('express');
const Parser = require('rss-parser');
const mysql = require('mysql');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', {title: 'Get some RSS'} );
});

// rss feed example: http://blog.aweber.com/feed
//                   http://rss.cnn.com/rss/cnn_topstories.rss
//                   http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml
router.post('/getFeed',(req, res) => {

    const con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "tofiga01",
        database: "rssparse"
    });

    let parser = new Parser();
    let parsedURL = req.body.feedURL;
    console.log(parsedURL);
    (async () => {
        feed = await parser.parseURL(parsedURL);
        let rssItems = [];
        con.connect(function (err) {
            feed.items.forEach(item => {
                rssItems.push(item);
                if(err) throw err;
                let sql = `INSERT INTO rss (feedTitle, itemTitle, itemLink, itemPubDate, itemCreator, itemContentSnippet, itemGuid, itemCategories) VALUES ("${feed.title}", "${item.title}", "${item.link}", "${item.pubDate}", "${item.creator}", "${item.contentSnippet}", "${item.guid}", "${item.categories}")`;
                con.query(sql,function (err) {
                    if (err) throw err;
                    console.log('affected 1 row');
                });
            });
            res.render('rss', {title: 'Parsed!', rssItems: rssItems});
        });
    })();
});

module.exports = router;