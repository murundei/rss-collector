const mysql = require('mysql');
const express = require('express');
const router = express.Router();


router.get('/',(req, res) => {
    const con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "tofiga01",
        database: "rssparse"
    });
    let parsedFeed = [];
    con.connect(function (err) {
        if (err) throw err;
        con.query("SELECT * FROM rss", function (err, result, fields) {
            if (err) throw err;
            parsedFeed.push(result);
            res.render('parsedRSS', {title: 'Parsed RSS', parsedFeed: parsedFeed});
        });
    });
});

module.exports = router;